package com.training.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.dto.StocksDTO;
import com.training.model.ProductEntity;
import com.training.repo.ProductRepo;



@Service
public class ProductService {

	@Autowired
	ProductRepo productRepo;
	
	public List<ProductEntity> getProducts(){
		return productRepo.findAll();
	}
	
	public List<ProductEntity> getProductsByCategory(String category){
		
	
		return productRepo.findByCategory(category);
		
	}
	public ProductEntity getProductById(Integer productId){
		return productRepo.findByProductId(productId);
	}
	public ProductEntity getProductByName(String name){
//		String match=name.substring(1, name.length()-1);
		ProductEntity prod= new ProductEntity();
		List<ProductEntity> productList=productRepo.findAll();
		for(ProductEntity product:productList) {
			if(product.getName().toLowerCase().equals(name.toLowerCase())) {
				prod= product;
			}
			
			
		}
		return prod;
	}
	public void addProduct(ProductEntity product) {
		product.setCategory(product.getCategory().toUpperCase());
		 productRepo.save(product);
	}
	
	public void updateProduct(ProductEntity product) {
		 productRepo.save(product);
	}
		 
		 public void deleteProduct(Integer productId) {
				productRepo.delete(productRepo.findByProductId(productId));
			}
			public List<StocksDTO> getProductStocks() {
				List<ProductEntity> products= productRepo.findAll();
				List<StocksDTO> productStocks = new ArrayList<>();
				products.forEach(product -> {
					productStocks.add(new StocksDTO(product.getName(), product.getStock()));
				});
				return productStocks;
			}
			
		
			
}
