package com.training.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.training.csv.CSVUpload;
import com.training.model.ProductEntity;
import com.training.repo.ProductRepo;
@Service
public class CSVService {
	@Autowired
	ProductRepo prodRepo;
	
	public void addProductsFromCSV(MultipartFile file) {
	    try {
	      List<ProductEntity> products = CSVUpload.csvToProducts(file.getInputStream());
	      System.out.println(products);
	      prodRepo.saveAll(products);
	    } catch (IOException e) {
	      throw new RuntimeException("fail to store csv data: " + e.getMessage());
	    }
	  }

}
