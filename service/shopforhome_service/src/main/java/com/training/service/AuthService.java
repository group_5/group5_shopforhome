package com.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.repo.AuthRepo;


import com.training.model.UserEntity;


@Service
public class AuthService {
	
	@Autowired
	AuthRepo authRepo;
	
	
	

	public Integer registerUser(UserEntity user) throws Exception {
		if(authRepo.findByUsername(user.getUsername()) != null)
			throw new Exception("User already exists");
		if(user.getUsername().isEmpty())
			throw new Exception("Username cannot be empty");
		if(user.getPassword().isEmpty())
			throw new Exception("Password cannot be empty");
		if(user.getRole() == null)
			user.setRole("USER");
		return authRepo.save(user).getUserId();
		
	
	
	}



}
