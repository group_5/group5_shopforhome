package com.training.service;

import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.training.model.UserEntity;

import com.training.repo.UserRepo;

@Service
public class UserService {
@Autowired
UserRepo userRepo;




public List<UserEntity> getUsers(){
	return userRepo.findByRole("USER");
}


public List<UserEntity> getUserById(Integer id){
	return userRepo.findByUserId(id);
}

public UserEntity getUserByUsername(String username) {
//	String match=username.substring(1, username.length()-1);
	return userRepo.findByUsername(username);
}


public void deleteUser(Integer userId) {
	userRepo.deleteById(userId);
}


public Integer updateUser(UserEntity user) {

	return userRepo.save(user).getUserId();
}

}
