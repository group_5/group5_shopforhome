package com.training.csv;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.springframework.web.multipart.MultipartFile;

import com.training.model.ProductEntity;

public class CSVUpload {
	public static String TYPE = "text/csv";
	static String[] HEADERs = { "Name", "Description", "Category", "Image", "Price", "Stock", "Rating" };

	public static boolean hasCSVFormat(MultipartFile file) {
		if (!TYPE.equals(file.getContentType())) {
			return false;
		}
		return true;
	}

	public static List<ProductEntity> csvToProducts(InputStream is) {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				CSVParser csvParser = new CSVParser(fileReader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

			List<ProductEntity> productList = new ArrayList<>();

			Iterable<CSVRecord> csvRecords = csvParser.getRecords();
			System.out.println(csvRecords);
			for (CSVRecord csvRecord : csvRecords) {
				ProductEntity product = new ProductEntity(

						csvRecord.get("Name"), csvRecord.get("Description"), csvRecord.get("Category"),

						csvRecord.get("Image"), Double.parseDouble(csvRecord.get("Price")),

						Integer.parseInt(csvRecord.get("Stock")), Integer.parseInt(csvRecord.get("Rating")));

				productList.add(product);
			}

			return productList;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		}
	}

}