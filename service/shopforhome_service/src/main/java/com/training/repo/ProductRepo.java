package com.training.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.model.ProductEntity;

@Repository
public interface ProductRepo extends JpaRepository<ProductEntity, Integer> {
	
	ProductEntity findByProductId(Integer productId);
	ProductEntity findByName(String name);
	List<ProductEntity> findByCategory(String category);
}
