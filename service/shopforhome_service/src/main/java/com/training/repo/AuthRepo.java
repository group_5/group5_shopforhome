package com.training.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.model.UserEntity;


@Repository
public interface AuthRepo extends JpaRepository<UserEntity, Integer>{
	public UserEntity findByUsername(String username);
}
