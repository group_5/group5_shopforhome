package com.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import com.training.csv.CSVUpload;
import com.training.dto.StocksDTO;
import com.training.model.ProductEntity;
import com.training.model.UserEntity;
import com.training.service.CSVService;
import com.training.service.ProductService;
import com.training.service.UserService;




@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AdminController {
	@Autowired
	UserService userService;
	
	@Autowired
	ProductService prodService;
	
	@Autowired
	CSVService fileService;
	
	@GetMapping("users")
	public List<UserEntity> getAllUsers(){
		
		return userService.getUsers();
	}
	
	@GetMapping("user")
	public List<UserEntity> getUserById(@RequestParam Integer userId){
	
		return userService.getUserById(userId);
	}
	@GetMapping("username")
	public UserEntity getUserById(@RequestParam String username){
	
		return userService.getUserByUsername(username);
	}
	
	@DeleteMapping("delete")
	public void deleteUser(@RequestParam Integer userId) {
		
		userService.deleteUser(userId);
	}
	
	@PutMapping("update/user")
	public Integer updateUser(@RequestBody UserEntity user) {
	
		return userService.updateUser(user);
	}
	@GetMapping("product")
	public ProductEntity getProductById(@RequestParam Integer productId){
	
		return prodService.getProductById(productId);
	}
	
	@PostMapping("add/product")
	public void addProduct(@RequestBody ProductEntity product) {
	
		 prodService.addProduct(product);
	}
	
	@PutMapping("update/product")
	public void updateProduct(@RequestBody ProductEntity product) {
		
		 prodService.updateProduct(product);
	}
	
	@DeleteMapping("delete/product")
	public void deleteProduct(@RequestParam Integer productId) {
		
		prodService.deleteProduct(productId);
	}
	
	@GetMapping("/stocks")
	public List<StocksDTO> getStocks() {
	
		return prodService.getProductStocks();
	}
	
	 @PostMapping("/upload")
	  public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
	    String message = "";
	    //System.out.println( (CSVUpload.hasCSVFormat(file)));
	    if (CSVUpload.hasCSVFormat(file)) {
	      try {
	        fileService.addProductsFromCSV(file);

	        message = "Uploaded the file successfully: " + file.getOriginalFilename();
	       

	        return ResponseEntity.status(HttpStatus.OK).body(message);
	      } catch (Exception e) {
	        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
	        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
	      }
	    }

	    message = "Please upload a csv file!";
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
	  }
}
