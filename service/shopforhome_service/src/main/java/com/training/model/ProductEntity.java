package com.training.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products", schema = "capstone_project")
public class ProductEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer productId;

	@Column
	private String name;

	@Column
	private double price;

	@Column
	private String description;
	@Column
	private String category;

	@Column
	private String image;
	@Column
	private int rating;

	@Column
	private int stock;
	
	public ProductEntity(String name,String description,String category, String image, double price,int stock,int rating) {
		this.name=name;
		this.description=description;
		this.category=category;
		this.image=image;
		this.price=price;
		this.stock=stock;
		this.rating=rating;
	}

}
