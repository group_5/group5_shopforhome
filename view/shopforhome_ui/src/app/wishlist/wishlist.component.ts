import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../service/user.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
public products:any=[];
  constructor(private service :UserServiceService) { }

  ngOnInit(): void {
    this.service.getWishlist()
    .subscribe(res=>{
      this.products=res;
      console.log(this.products)
    })
  }

}
