export class Product{
    constructor(
        public productId:string,
        public name:string,
        public category:string,
        public description:string,
        public image:string,
       
        public price:any,
        public stock:number,
        public rating:number,
        public quantity:number=1

    ){}
}