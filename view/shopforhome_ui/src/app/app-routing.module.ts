import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { ProductCategoryComponent } from './product-category/product-category.component';

import { CartComponent } from './cart/cart.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { BulkUploadComponent } from './admin/bulk-upload/bulk-upload.component';

const routes: Routes = [
  {
    path:'home',component:HomeComponent
  },
  {
    path:'login',component:LoginComponent
  },
  {
    path:'register',component:RegisterComponent
  },
  {
    path:'cart',component:CartComponent
  },
  {
  path:'products',component:ProductsComponent
},
{
  path:'admin',component:AdminComponent
},
{
  path:'wishlist',component:WishlistComponent
},
{
  path:'search',component:SearchComponent
},
{
  path:'admin/upload/products',component:BulkUploadComponent
},


{
  path:'product/category',component:ProductCategoryComponent
}



  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
