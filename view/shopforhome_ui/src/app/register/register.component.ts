import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  register!: FormGroup;
  retData: any;
  constructor(  private formBuilder: FormBuilder,private http: HttpClient,
    private router: Router) { }

  ngOnInit(): void {
    this.register = this.formBuilder.group({
      username: [''],
      password: [''],
      email:[''],
      name:['']
    });
  }
  registerUser() {
    if (
      this.register.value.userame === null ||
      this.register.value.username === ''
    ) {
      alert('Username is a required field.');
    } else if (
      this.register.value.password === null ||
      this.register.value.password === ''
    ) {
      alert('Password is a required field.');
    }else{
      const headers = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
      this.http.post<any>('http://localhost:8080/register', { username: this.register.value.userName, password: this.register.value.password,email:this.register.value.email,name:this.register.value.name}, {headers}).subscribe(data => {
        this.retData = JSON.stringify(data.role);
        console.log(data)
        window.sessionStorage.setItem('login', 'true');
        
        this.router.navigate(['/home']);
    })
    }
  }
}
