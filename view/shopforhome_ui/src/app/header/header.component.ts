import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { RestService } from '../service/rest.service';
import { UserServiceService } from '../service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
public  search:string="";

public product:any;
  constructor(private router:Router, private auth:AuthService,private service:UserServiceService) { }

  ngOnInit(): void {
  }
  isLoggedIn(){
    return this.auth.isAuthorized;
  }

  logout(){
    if(confirm("Are you sure you want to log out?")){
      window.localStorage.removeItem("login");
      window.localStorage.removeItem("role");
      this.auth.isAuthorized=false;
      this.auth.isUser=null
      this.router.navigate(["/home"])
    }
  }

  isUser(){
    return this.auth.isUser;
  }

  searchItem(search: string){
   this.service.search(search)
   this.router.navigate(['search'])

  }
}
