import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from '../header/header.component';
import { RestService } from '../service/rest.service';
import { UserServiceService } from '../service/user.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
product:any;
  constructor(private service:UserServiceService,private rest:RestService) { }

  ngOnInit(): void {
   this.service.getSearch().subscribe((res: any)=>{
    this.product=res[0]
    console.log(res[0])
   })

}

}
