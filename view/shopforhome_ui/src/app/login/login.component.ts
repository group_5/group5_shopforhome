import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm!:FormGroup
role:any
  constructor(private auth:AuthService, private formBuilder:FormBuilder, private http:HttpClient,private router:Router) { }

  ngOnInit(): void {
    this.loginForm=this.formBuilder.group({
      username:[''],
      password:['']
    });
  }
   login(){
    if (
      this.loginForm.value.username === null ||
      this.loginForm.value.username === ''
    ) {
      alert('Username is a required field.');
    } else if (
      this.loginForm.value.password === null ||
      this.loginForm.value.password === ''
    ) {
      alert('Password is a required field.');
    }else{
      const headers = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
      this.http.post<any>('http://localhost:8080/login', { username: this.loginForm.value.username, password: this.loginForm.value.password}, {headers}).subscribe(data => {
        this.role = data.role;
        console.log(this.role)
        this.auth.setToken(data.authToken)
        console.log(this.auth.getToken())
        localStorage.setItem('login', "true");
        localStorage.setItem('role', this.role);
        this.auth.isAuthorized=true;
        // this.loginForm.reset();
        if("ADMIN" === this.role){
          this.router.navigate(['/admin']);
          this.auth.isUser=false
        } else {
          this.router.navigate(['/home']);
          this.auth.isUser=true;
        }
    })
    }
  }

}
