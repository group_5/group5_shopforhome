import { Component, OnInit } from '@angular/core';
import { RestService } from '../service/rest.service';
import { UserServiceService } from '../service/user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
productList:any=[]
public total!:number;
  constructor(private service :UserServiceService) { }

  ngOnInit(): void {
    this.service.getCartItem()
    .subscribe(res=>{
      this.productList=res;
      console.log(this.productList)
     this.total=this.service.getTotal();
    })
  }

    removeItem(item:any){
      this.service.removeCartItem(item);
    }
  
    emptycart(){
      this.service.emptyCart();
    }
    increaseQuantity(item: any){
      console.log(item)
      item.quantity+=1;
      this.total+=item.price
    }
  
    decreaseQuantity(item: any){
      if(item.quantity != 0)
        item.quantity-=1;
        this.total-=item.price
    }

   

}


