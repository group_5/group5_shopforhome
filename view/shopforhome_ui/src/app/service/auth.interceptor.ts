import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http"
import { catchError, Observable, throwError } from "rxjs";
import { AuthService } from "./auth.service";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
    
    constructor(private authService:AuthService){}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       if(req.headers.get('No-Auth')=='True'){
return next.handle(req.clone())
       }

       const token =this.authService.getToken();
       this.addToken(req,token)
       return next.handle(req).pipe(
        catchError(
            (err:HttpErrorResponse)=>{
                return throwError("Something went wrong")
            }
            )
            
        )
    }
    
private addToken(request:HttpRequest<any>,token:any){
    return request.clone(
        {
            setHeaders:{
                Authorization: `Bearer ${token}`
            }
        }
    );
    
}
    }
