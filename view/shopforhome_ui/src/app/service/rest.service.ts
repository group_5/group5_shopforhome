import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { generate, map } from 'rxjs';
import { AuthService } from './auth.service';
import { Product } from '../products/products-model';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient,private auth:AuthService) { }
requestHeader=new HttpHeaders(
  {
    "No-Auth":"True"
  }
)
  public generateToken(request:any){
  return this.http.post("http://localhost:8080/login",request,{headers:this.requestHeader});
  }
  public authHeaders(){
    let tokenStr='Bearer '+ this.auth.getToken();
    const headers=new HttpHeaders().set("Authorization",tokenStr);
  return headers;
  }


  getProduct(){
   
    return this.http.get<Product[]>("http://localhost:8080/products",{headers: this.authHeaders()})
    .pipe(map((res:Product[])=>{
      console.log(res)
      return res;
    }))
    }
getProductByCategory(category:string){
  return  this.http.get<any>("http://localhost:8080/productsByCategory/?category=".concat(category),{headers: this.authHeaders()})
  .pipe(map((res:any)=>{
    console.log(res)
    return res
    
  }))

}
    search(name:string){
      return  this.http.get<any>("http://localhost:8080/productByname/?name=".concat(name),{headers: this.authHeaders()})
      .pipe(map((res:any)=>{
        console.log(res)
        return res
      }))
    }

    getUsers(){
      
    return this.http.get<any>("http://localhost:8080/users",{headers: this.authHeaders()})
    .pipe(map((res:any)=>{
      console.log(res)
      return res;
    }))
    }
}
