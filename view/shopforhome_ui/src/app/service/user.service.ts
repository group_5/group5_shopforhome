import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { RestService } from './rest.service';
import { BehaviorSubject } from 'rxjs';
import { Product } from '../products/products-model';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  public cartItemList : Product[]=[]
  public wishList :Product[]=[]
  public productList = new BehaviorSubject<Product[]>([]);
  public productWishList = new BehaviorSubject<Product[]>([]);
  public searchItem:any
  public searchItemList:any=[]
  public passSearchItem=new BehaviorSubject<any>([])
  constructor(private http: HttpClient,private rest:RestService) { }

  
search(search:string){
  this.rest.search(search).subscribe(res=>{
    this.searchItem=res;
    this.searchItemList.push(this.searchItem)
    this.passSearchItem.next(this.searchItemList)
    console.log(this.searchItem)
    

  })
}
  
   
   getSearch(){
    return this.passSearchItem.asObservable();
   }
  
    getWishlist(){
      return this.productWishList.asObservable();
    }
  
    
  
    addtoCart(product : any){
      if(this.cartItemList.includes(product)){
      alert("Item already in cart")
      }else{
        this.cartItemList.push(product);
      this.productList.next(this.cartItemList)
      }
      
      
      console.log(this.cartItemList)
    }
  
    addToWishlist(product : any){
      if(this.wishList.includes(product)){
        alert("Item already in wishlist")
      }
     else
     this.wishList.push(product);
      this.productWishList.next(this.wishList)
    }

    getCartItem(){
      return this.productList.asObservable();
    }
    removeCartItem(product: any){
      this.cartItemList.map((a:any, index:any)=>{
        if(product.id== a.id){
          this.cartItemList.splice(index,1);
        }
      })
  
      this.productList.next(this.cartItemList);
    }
  
    removeWishListItem(product: any){
      this.wishList.map((a:any, index:any)=>{
        if(product.id== a.id){
          this.wishList.splice(index,1);
        }
      })
  
      this.productWishList.next(this.wishList);
    }
  
    removeAllWishlist(){
      this.wishList = []
      this.productWishList.next(this.wishList);
    }
  
  
    emptyCart(){
      this.cartItemList = []
      this.productList.next(this.cartItemList);
    }

    getTotal() : number{
      let grandTotal = 0;
      this.cartItemList.map((a:any) =>{
        grandTotal += a.total;
      })
      return grandTotal;
    }
   

   
}
