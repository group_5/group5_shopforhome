import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
public isAuthorized:boolean=false;
public isUser:any
  constructor() { }
public setToken(authToken:string){
  localStorage.setItem("authToken",authToken)
}
  public getToken(){
    return localStorage.getItem("authToken")
  }
}
