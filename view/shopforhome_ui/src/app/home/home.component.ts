import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Product } from '../products/products-model';
import { AuthService } from '../service/auth.service';
import { RestService } from '../service/rest.service';
import { UserServiceService } from '../service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public productList:Product[]=[];
  public static category:string=""
  constructor(private service:UserServiceService,private auth:AuthService,private router:Router,private rest:RestService) { }

  ngOnInit(): void {
    this.rest.getProduct()
    .subscribe(res=>{
      this.productList= res;
      console.log(this.productList)
        
      this.productList.forEach((a:any)=>{
        Object.assign(a,{quantity:1,total:a.price});
        console.log(a.quantity)
      })
      
    })
  }
  isLoggedIn(){
    return this.auth.isAuthorized;
  }
  addToCart(item: any){
    if(this.isLoggedIn()){
      
      
      this.service.addtoCart(item);
    }else{
      alert("You must be logged in to add items in your cart!!!")
      this.router.navigate(['/login']);
    }
  }
 

  addToWishlist(item: any){
    if(this.isLoggedIn()){
      this.service.addToWishlist(item);
    }else{
      alert("You must be logged in to add items in your cart!!!")
      this.router.navigate(['/login']);
    }
  }
  showCategory(category: string){
    HomeComponent.category = category;
    this.router.navigate(['/product/category']);
  }

}
